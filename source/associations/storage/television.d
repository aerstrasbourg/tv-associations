/++
    Authors: Matthieu Kern, matthieukern@gmail.com
    Date: May 20, 2015
    Copyright: Matthieu Kern
	License: Subject to the terms of the MIT license, as written in the included LICENSE file.
 +/
module associations.storage.television;

import mongorm;

/++
    A class that represents a television in the datastore.
 +/
class STelevision : Storable {
public:
    mixin(Store!STelevision);

    /++
        The television ip.
     +/
    @update @property void ip(string arg) { _ip = arg; }

    ///ditto
    @property string ip() { return _ip; }

    /++
        The television name.
     +/
    @update @property void name(string arg) { _name = arg; }

    ///ditto
    @property string name() { return _name; }

    /++
        The television description.
     +/
    @update @property void description(string arg) { _desc = arg; }

    ///ditto
    @property string description() { return _desc; }

    /++
        The television associated composition identifier.
     +/
    @update @property void composition(BsonObjectID arg) { _composition = arg; }

    ///ditto
    @property BsonObjectID composition() { return _composition; }

private:
    string _ip;
    string _name;
    string _desc;
    BsonObjectID _composition;
}

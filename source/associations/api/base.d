/++
    Authors: Matthieu Kern, matthieukern@gmail.com
    Date: May 20, 2015
    Copyright: Matthieu Kern
	License: Subject to the terms of the MIT license, as written in the included LICENSE file.
 +/
module associations.api.base;

import vibe.d;

import associations.utils.error;
import associations.api.television;

/++
    Base API interface that defines all the first level api.
 +/
interface APIBaseInterface {
    /++
        $(ROUTE $(GET), /)

        The base televisions route
     +/
    @path("/")
    @property APITelevisionInterface television();
}

/++
    Base API class that implements base interface.

    This is the enter point of the API. This class manage the calling
    of all the others API classes, like APIUsers.

    It must be used in the URLRouter when launching the server.
 +/
class APIBase : APIBaseInterface {
private:
    APITelevision _televisions;

public:
    /++
        Init all the api subclasses.
     +/
    this() {
        _televisions = new APITelevision();
    }

override:
    /++ $(ROUTE $(GET), /) +/
    @property APITelevisionInterface television() { return _televisions; }
}

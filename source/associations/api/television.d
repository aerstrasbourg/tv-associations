/++
    Authors: Matthieu Kern, matthieukern@gmail.com
    Date: May 20, 2015
    Copyright: Matthieu Kern
	License: Subject to the terms of the MIT license, as written in the included LICENSE file.
 +/
module associations.api.television;

import std.typecons;

import vibe.d;

import associations.storage;
import associations.utils;

/++ Television API interface that define televisions relative methods. +/
interface APITelevisionInterface {
    /++
        $(ROUTE $(POST), /television)

        Create a new television.

        Request_body:
        ---
            {
                "ip": "192.168.0.1",
                "name": "The television name",
                "description": "The television description",
                "composition": "555b45872e13a7b960000000"
            }
        ---

        Response_body:
        ---
            {
                "_id": "555c9934f8080e5d2d000000",
                "ip": "192.168.0.1",
                "name": "The television name",
                "description": "The television description",
                "composition": {
                    ...
                }
            }
        ---
     +/
    Json addTelevision(string ip, string name, string description, string composition);

    /++
        $(ROUTE $(GET), /television)

        Get all televisions.

        Request_body:
        ---
            {
            }
        ---

        Response_body:
        ---
            [
                {
                    "_id": "555c9934f8080e5d2d000000",
                    "ip": "192.168.0.1",
                    "name": "The television name",
                    "description": "The television description",
                    "composition": {
                        ...
                    }
                },
                ...
            ]
        ---
     +/
    Json getTelevision();

    /++
        $(ROUTE $(GET), /television/:id)

        Get a television by it's id.

        Request_body:
        ---
            {
            }
        ---

        Response_body:
        ---
            {
                "_id": "555c9934f8080e5d2d000000",
                "ip": "192.168.0.1",
                "name": "The television name",
                "description": "The television description",
                "composition": {
                    ...
                }
            }
        ---
     +/
    @path("/television/:id")
    Json getTelevisionById(string _id);

    /++
        $(ROUTE $(PATCH), /television/:id)

        Update a television.

        Request_body:
        ---
            {
                "ip": "192.168.0.1",
                "name": "The television name",
                "description": "The television description",
                "composition": "555b45872e13a7b960000000"
            }
        ---

        Response_body:
        ---
            {

                "_id": "555c9934f8080e5d2d000000",
                "ip": "192.168.0.1",
                "name": "The television name",
                "description": "The television description",
                "composition": {
                    ...
                }
            }
        ---
     +/
    @path("/television/:id")
    Json updateTelevision(string _id, string ip, string name, string description, string composition);

    /++
        $(ROUTE $(DELETE) /television/:id)

        Delete a television by it's id.

        Request_body:
        ---
            {
            }
        ---

        Response_body:
        ---
            {
            }
        ---
     +/
    @path("/television/:id")
    Json deleteTelevision(string _id);
}

/++ Implements the televisions API. +/
class APITelevision : APITelevisionInterface {
public:
    this() {}

override:
    /++ $(ROUTE $(POST), /television) +/
    Json addTelevision(string ip, string name, string description, string composition) {

        logInfo("Composition:");
        logInfo(composition);

        auto television = new STelevision;
        television.ip = ip;
        television.name = name;
        television.description = description;
        television.composition = BsonObjectID.fromString(composition);

        Json ret = serializeToJson(television);

        try {
            requestHTTP(Params.get("COMPOSITIONS_ADDRESS") ~ "/composition/" ~ ret.composition.get!string,
    			(scope req){},
    			(scope res){
                    if (res.statusCode >= 200 && res.statusCode < 300)
                        ret.composition = res.readJson;
                    else
                        ret.composition = Json.emptyObject;
    			}
    		);
        } catch (Exception) {
            ret.composition = Json.emptyObject;
        }

        if (composition != "000000000000000000000000") {
            requestHTTP(television.ip ~ Params.get("TV_UPDATE_ROUTE"),

                (scope req){
                    req.method = HTTPMethod.POST;
                    req.writeJsonBody(ret.composition);
                },
                (scope res){
                    if (res.statusCode < 200 || res.statusCode >= 300)
                        throw new Exception("Cannot update TV...");
                }
            );
        }

        television.store();
        ret._id = serializeToJson(television)._id;

        return ret;
    }

    /++ $(ROUTE $(GET), /television) +/
    Json getTelevision() {
        auto televisions = STelevision.getFromDatastore(Bson.emptyObject);

        Json ret = Json.emptyArray;

        foreach (STelevision t ; televisions) {
            Json tv = serializeToJson(t);
            try {
                requestHTTP(Params.get("COMPOSITIONS_ADDRESS") ~ "/composition/" ~ tv.composition.get!string,
                    (scope req){},
                    (scope res){
                        if (res.statusCode >= 200 && res.statusCode < 300)
                            tv.composition = res.readJson;
                        else
                            tv.composition = Json.emptyObject;
                    }
                );
            } catch (Exception) {
                tv.composition = Json.emptyObject;
            }
            ret ~= tv;
        }

        return ret;
    }

    /++ $(ROUTE $(GET), /television/:id) +/
    Json getTelevisionById(string _id) {
        auto television = STelevision.getById(_id);

        Json ret = serializeToJson(television);

        try {
            requestHTTP(Params.get("COMPOSITIONS_ADDRESS") ~ "/composition/" ~ ret.composition.get!string,
    			(scope req){},
    			(scope res){
                    if (res.statusCode >= 200 && res.statusCode < 300)
			            ret.composition = res.readJson;
                    else
                        ret.composition = Json.emptyObject;
    			}
    		);
        } catch (Exception) {
            ret.composition = Json.emptyObject;
        }

        return ret;
    }

    /++ $(ROUTE $(PATCH), /television/:id) +/
    @path("/television/:id")
    Json updateTelevision(string _id, string ip, string name, string description, string composition) {
        auto television = STelevision.getById(_id);
        if (ip)
            television.ip = ip;
        if (name)
            television.name = name;
        if (description)
            television.description = description;
        if (composition)
            television.composition = BsonObjectID.fromString(composition);

        Json ret = serializeToJson(television);

        try {
            requestHTTP(Params.get("COMPOSITIONS_ADDRESS") ~ "/composition/" ~ ret.composition.get!string,
    			(scope req){},
    			(scope res){
                    if (res.statusCode >= 200 && res.statusCode < 300)
			            ret.composition = res.readJson;
                    else
                        ret.composition = Json.emptyObject;
    			}
    		);
        } catch (Exception) {
            ret.composition = Json.emptyObject;
        }

        requestHTTP(television.ip ~ Params.get("TV_UPDATE_ROUTE"),
            (scope req){
                req.method = HTTPMethod.POST;
                req.writeJsonBody(ret.composition);
            },
            (scope res){
                if (res.statusCode < 200 || res.statusCode >= 300)
                    throw new Exception("Cannot update TV...");
            }
        );

        television.store();

        return ret;
    }

    /++ $(ROUTE $(DELETE), /television/:id) +/
    Json deleteTelevision(string _id) {
        auto television = STelevision.getById(_id);
        television.remove();
        return Json.emptyObject;
    }
}

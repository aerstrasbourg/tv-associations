# Associations
A microservice to associate some compositions to a television.

# API
### POST /television

Create a new television.

Request body:
```
{
    "ip": "192.168.0.1",
    "name": "The television name",
    "description": "The television description",
    "composition": "555b45872e13a7b960000000"
}
```

Response body:
```
{
    "_id": "555c9934f8080e5d2d000000",
    "ip": "192.168.0.1",
    "name": "The television name",
    "description": "The television description",
    "composition": {
        ...
    }
}
```

### GET /television

Get all televisions.

Request body:
```
{
}
```

Response body:
```
[
    {
        "_id": "555c9934f8080e5d2d000000",
        "ip": "192.168.0.1",
        "name": "The television name",
        "description": "The television description",
        "composition": {
            ...
        }
    },
    ...
]
```

### GET /television/:id

Get a television by it's id.

Request body:
```
{
}
```

Response body:
```
{
    "_id": "555c9934f8080e5d2d000000",
    "ip": "192.168.0.1",
    "name": "The television name",
    "description": "The television description",
    "composition": {
        ...
    }
}
```

### PATCH /television/:id

Update a television.

Request body:
```
{
    "ip": "192.168.0.1",
    "name": "The television name",
    "description": "The television description",
    "composition": "555b45872e13a7b960000000"
}
```

Response body:
```
{

    "_id": "555c9934f8080e5d2d000000",
    "ip": "192.168.0.1",
    "name": "The television name",
    "description": "The television description",
    "composition": {
        ...
    }
}
```

### DELETE /television/:id

Delete a television by it's id.

Request body:
```
{
}
```

Response body:
```
{
}
```

# Documentation
A [documentation](aerstrasbourg.bitbucket.org/tv-associations/) is available for further informations.

# Author
Developed by Matthieu KERN, matthieukern@gmail.com
